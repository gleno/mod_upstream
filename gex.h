
typedef struct {
	double stability;
	double confidence;
	int is_final;
	char *transcription;
} transcription_result_t;

typedef struct {
	int count;
	transcription_result_t** data;
} transctiption_response_t;


typedef void (*response_callback_t)(transctiption_response_t*, void*);

#ifdef __cplusplus
extern "C" {
#endif	
	void gex_init_module();
	void gex_shutdown_module();
	void *gex_start(uintptr_t samples_per_second, response_callback_t cb, void *ctx);
	void gex_stt(void *gex_instance, uint8_t *buffer, size_t n);
	void gex_stop(void *gex_instance);
	char *gex_text(void *gex_instance);

#ifdef __cplusplus
}
#endif
