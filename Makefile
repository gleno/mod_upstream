CFLAGS=-ggdb -fPIC -pthread -lgrpc++ -lstdc++ -lfreeswitch -I/usr/include/freeswitch -Wall -Werror -Wfatal-errors
CXXFLAGS=-g -ggdb -pthread -fPIC -std=c++14 -Iinclude/ -Igrpc/include -Wall -Werror -Wfatal-errors

all: mod_upstream.so

mod_upstream.so: mod_upstream.o gex.o
	$(CC) mod_upstream.o gex.o libgoogle.a libprotobuf.a $(CFLAGS) -shared -o mod_upstream.so
	sudo cp mod_upstream.so /usr/lib/freeswitch/mod/

mod_upstream.o:
	$(CC) mod_upstream.c -c $(CFLAGS) -o mod_upstream.o

gex.o: 
	$(CXX) -c gex.cpp $(CXXFLAGS) -o gex.o

.PHONY: clean
clean:
	rm -f gex.o mod_upstream.o mod_upstream.so