#include <switch.h>
#include "gex.h"

SWITCH_MODULE_RUNTIME_FUNCTION(mod_upstream_runtime);
SWITCH_MODULE_LOAD_FUNCTION(mod_upstream_load);
SWITCH_MODULE_SHUTDOWN_FUNCTION(mod_upstream_shutdown);
SWITCH_MODULE_DEFINITION(mod_upstream, mod_upstream_load, mod_upstream_shutdown, NULL);


typedef struct {
	switch_media_bug_t *bug;
	void *gex_instance;
	char *name;
} side_t;

static switch_bool_t capture_callback(switch_media_bug_t *bug, void *user_data, switch_abc_type_t type) {
	side_t *side = (side_t *) user_data;
	switch_core_session_t *session = switch_core_media_bug_get_session(bug);
	switch_channel_t *channel = switch_core_session_get_channel(session);

	switch (type) {

	case SWITCH_ABC_TYPE_CLOSE:
		{
			char *text = gex_text(side->gex_instance);
			text = switch_core_session_sprintf(session, "%s", text);

			switch_event_t *event;
			switch_event_create_subclass(&event, SWITCH_EVENT_CUSTOM, "UPSTREAM::TRANSCRIPT");
			switch_event_add_header_string(event, SWITCH_STACK_BOTTOM, "Unique-ID", switch_core_session_get_uuid(session));
			switch_event_add_header_string(event, SWITCH_STACK_BOTTOM, "transcript", text);
			switch_event_add_header_string(event, SWITCH_STACK_BOTTOM, "side", side->name);			

			switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_NOTICE, "Upstream Transcript (%s): %s\n", side->name, text);
			switch_event_fire(&event);

			gex_stop(side->gex_instance);
			switch_channel_set_private(channel, "upstream", NULL);
		}

		break;

	case SWITCH_ABC_TYPE_WRITE: 		
	case SWITCH_ABC_TYPE_READ:
		{
			uint8_t data[SWITCH_RECOMMENDED_BUFFER_SIZE];
			switch_frame_t frame = { 0 };

			frame.data = data;
			frame.buflen = SWITCH_RECOMMENDED_BUFFER_SIZE;

			while (switch_core_media_bug_read(bug, &frame, SWITCH_TRUE) == SWITCH_STATUS_SUCCESS && !switch_test_flag((&frame), SFF_CNG)) {
				if (frame.datalen) {
					gex_stt(side->gex_instance, frame.data, frame.datalen);
				}
			}
		}
		break;

	default:
		break;
	}

	return SWITCH_TRUE;
}

static void response_callback(transctiption_response_t *response, void *user_data) {

	side_t *side = (side_t *) user_data;
	switch_core_session_t *session = switch_core_media_bug_get_session(side->bug);

	for(int i = 0; i < response->count; ++i){
		transcription_result_t* result = response->data[i];

		switch_event_t *event;
		switch_event_create_subclass(&event, SWITCH_EVENT_CUSTOM, "UPSTREAM::STT");
		switch_event_add_header_string(event, SWITCH_STACK_BOTTOM, "Unique-ID", switch_core_session_get_uuid(session));
		switch_event_add_header_string(event, SWITCH_STACK_BOTTOM, "side", side->name);

		char *stb_v = switch_core_session_sprintf(session, "%f", result->stability);
		switch_event_add_header_string(event, SWITCH_STACK_BOTTOM, "stability", stb_v);

		char *stt_v = switch_core_session_sprintf(session, "%s", result->transcription);
		switch_event_add_header_string(event, SWITCH_STACK_BOTTOM, "text", stt_v);

		char *con_v = switch_core_session_sprintf(session, "%f", result->confidence);
		switch_event_add_header_string(event, SWITCH_STACK_BOTTOM, "confidence", con_v);

		char *fin_v = switch_core_session_sprintf(session, "%s", result->is_final == 1 ? "yes" : "no");
		switch_event_add_header_string(event, SWITCH_STACK_BOTTOM, "final", fin_v);

		switch_event_fire(&event);
		switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_NOTICE, "Talk (%s): %s\n", side->name, stt_v);
	}
}

#define UPSTREAM_START_SYNTAX ""
SWITCH_STANDARD_APP(upstream_app) {
	switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_NOTICE, "Upstream Invoked\n");
	switch_channel_t *channel = switch_core_session_get_channel(session);

	if (switch_channel_get_private(channel, "upstream")) {
		switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_ERROR, "Upstream Bug Already Set\n");
		return;
	}

	if (switch_channel_pre_answer(channel) != SWITCH_STATUS_SUCCESS) {
		switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_ERROR, "Upstream Not Pre Answer?\n");
		return;
	}

	switch_codec_implementation_t read_impl = { 0 };
	switch_core_session_get_read_impl(session, &read_impl);

	side_t 	*a = switch_core_session_alloc(session, sizeof(side_t)),
			*b = switch_core_session_alloc(session, sizeof(side_t));

	a->gex_instance = gex_start(read_impl.samples_per_second, response_callback, a);
	b->gex_instance = gex_start(read_impl.samples_per_second, response_callback, b);

	a->name = "A";
	b->name = "B";

	{
		switch_media_bug_flag_t flags = SMBF_READ_STREAM | SMBF_READ_PING;
		if ((switch_core_media_bug_add(session, "upstream", NULL, capture_callback, a, 0, flags, &a->bug)) != SWITCH_STATUS_SUCCESS) {
			switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_ERROR, "Upstream: Unable to Attach Bug\n");
			return;
		}
	}

	{
		switch_media_bug_flag_t flags = SMBF_WRITE_STREAM | SMBF_READ_PING;
		if ((switch_core_media_bug_add(session, "upstream", NULL, capture_callback, b, 0, flags, &b->bug)) != SWITCH_STATUS_SUCCESS) {
			switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_ERROR, "Upstream: Unable to Attach Bug\n");
			return;
		}
	}

	switch_ivr_bind_dtmf_meta_session(session, 7, SBF_DIAL_ALEG | SBF_EXEC_ALEG | SBF_EXEC_SAME, "upstream::dtmf");
	switch_channel_set_private(channel, "upstream", a);
	switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_NOTICE, "Upstream Started\n");
	return;
}

SWITCH_MODULE_LOAD_FUNCTION(mod_upstream_load) {
	switch_application_interface_t *app_interface;
	*module_interface = switch_loadable_module_create_module_interface(pool, modname);
	SWITCH_ADD_APP(app_interface, "upstream", "Upwire Stream APP", "", upstream_app, UPSTREAM_START_SYNTAX, SAF_MEDIA_TAP);
	gex_init_module();
	return SWITCH_STATUS_SUCCESS;
}

SWITCH_MODULE_SHUTDOWN_FUNCTION(mod_upstream_shutdown){
	gex_shutdown_module();
	return SWITCH_STATUS_SUCCESS;
}