echo "Cleaning Up"
rm mod_upstream.o
rm mod_upstream.so
rm gex.o

echo "Compiling GEX"
g++ -ggdb -fPIC -o gex.o -c gex.cpp -std=c++11 -Wfatal-errors -pthread -I../include/ -I../libs/grpc/include

echo "Compiling mod_upstream"
cc mod_upstream.c -ggdb -c -fPIC -g -ggdb -I/usr/include/freeswitch -Wall -Werror -o mod_upstream.o
cc mod_upstream.o -ggdb -fPIC -shared gex.o libgoogle.a libprotobuf.a -lgrpc++ -lstdc++ -lfreeswitch -Wall -Werror -o mod_upstream.so -pthread

echo "All Done"

sudo cp mod_upstream.so /usr/lib/freeswitch/mod/
##sudo /etc/init.d/freeswitch restart
echo "OK"