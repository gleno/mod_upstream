#include <string.h>

#include <grpc/grpc_posix.h>
#include <grpc++/grpc++.h>

#include <chrono>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <thread>
#include <mutex>
#include <sstream>

#include "google/protobuf/stubs/common.h"
#include "google/cloud/speech/v1/cloud_speech.grpc.pb.h"
#include "gex.h"

using google::cloud::speech::v1::RecognitionConfig;
using google::cloud::speech::v1::Speech;
using google::cloud::speech::v1::StreamingRecognizeRequest;
using google::cloud::speech::v1::StreamingRecognizeResponse;
using google::cloud::speech::v1::SpeechRecognitionAlternative;

typedef grpc::ClientReaderWriterInterface<StreamingRecognizeRequest, StreamingRecognizeResponse> streamer_t;

struct GoogleIntercom {
	
	std::unique_ptr<streamer_t> streamer;
	GoogleIntercom() {
		this->creds = grpc::GoogleDefaultCredentials();
		this->channel =  grpc::CreateChannel("speech.googleapis.com", creds);
		this->speech = Speech::NewStub(channel);
		this->context = std::unique_ptr<grpc::ClientContext>(new grpc::ClientContext());
		this->streamer = speech->StreamingRecognize(context.get());
	}
	private:
	std::shared_ptr<grpc::ChannelCredentials> creds;
	std::shared_ptr<grpc::Channel> channel;
	std::unique_ptr<Speech::Stub> speech;
	std::unique_ptr<grpc::ClientContext> context;
};

struct gex_instance_t {
	// C-World
	void* ctx;
	response_callback_t cb;

	//Stream Settings
	uint samples_per_second;

	// Internal State
	GoogleIntercom *intercom;
	std::thread* wth;
	std::thread* rth;
	std::vector<uint8_t*> audio_bits;
	std::vector<char *> allocated_strings;
	std::mutex wm;

	char *text = nullptr;
};

template<typename T, typename Q>
static void populate_from_vector(const std::vector<T*>& v, Q* q) {
	q->data = new T*[q->count = v.size()];
	for(int i = 0; i < q->count; ++i)
		q->data[i] = v[i];
}

static char* to_c_string(gex_instance_t *instance, const std::string& str){
	char *cstr = new char[str.length() + 1];
	instance->allocated_strings.push_back(cstr);
	strcpy(cstr, str.c_str());
	return cstr;
}

char *gex_text(void *gex_instance){
	gex_instance_t *instance = static_cast<gex_instance_t*>(gex_instance);
	//instance->rth->join(); //Must Have compelted Read OP
	return instance->text;
}

static void read_thread(gex_instance_t *instance) {

	auto streamer = instance->intercom->streamer.get();
	StreamingRecognizeResponse response;

	while (streamer->Read(&response)) { 
		std::vector<transcription_result_t*> tresults;
		for (int r = 0; r < response.results_size(); ++r) {

			auto result = response.results(r);

			if(result.is_final()) {				

				std::vector<SpeechRecognitionAlternative> alts;
				for (int i = 0; i < result.alternatives_size(); ++i)
					alts.push_back(result.alternatives(i));

				auto idx = std::max_element(alts.begin(), alts.end(), 
					[](auto& a, auto& b){
						return (a.confidence()) < (b.confidence());
					});
				
				auto malt = alts[std::distance(alts.begin(), idx)];
				auto tr = new transcription_result_t();

				tr->is_final = result.is_final() ? 1 : 0;
				tr->stability = result.stability();
				tr->confidence = malt.confidence();
				tr->transcription = to_c_string(instance, malt.transcript());
				tresults.push_back(tr);
				instance->text = tr->transcription;
			}
		}

		auto tresponse = new transctiption_response_t();
		populate_from_vector(tresults, tresponse);

		instance->cb(tresponse, instance->ctx);

		for(int i = 0; i < tresponse->count; ++i)
			delete tresponse->data[i];
		delete tresponse;
	}
}

static void write_thread(gex_instance_t *instance) {
	auto streamer = instance->intercom->streamer.get();
	StreamingRecognizeRequest request;
	auto* streaming_config = request.mutable_streaming_config();
	auto config = streaming_config->mutable_config();

	config->set_language_code("en");
	config->set_sample_rate_hertz(instance->samples_per_second);
	config->set_encoding(RecognitionConfig::LINEAR16);
	//streaming_config->set_interim_results(true);
	config->set_profanity_filter(true);

	instance->wm.lock();
	streamer->Write(request);
	instance->wm.unlock();
}

void gex_stt(void *gex_instance, uint8_t* buffer, size_t n){
	gex_instance_t *instance = static_cast<gex_instance_t*>(gex_instance);
	auto streamer = instance->intercom->streamer.get();
	StreamingRecognizeRequest srr;
	srr.set_audio_content(buffer, n);

	instance->wm.lock();
	streamer->Write(srr);
	instance->wm.unlock();
}

void gex_init_module() {
	// GDB: handle SIG44 noprint nostop
	grpc_use_signal(44);
}

void* gex_start(uintptr_t samples_per_second, response_callback_t cb, void* ctx){

	// Construct new GEX Instance
	auto instance = new gex_instance_t();
	instance->samples_per_second = static_cast<uint>(samples_per_second);
	instance->cb = cb;
	instance->ctx = ctx;
	instance->intercom = new GoogleIntercom();

	// Initialize Google Speech API Streamer
	instance->wth = new std::thread(&write_thread, instance);
	instance->rth = new std::thread(&read_thread, instance);

	return static_cast<void*>(instance);
}

void gex_stop(void *gex_instance){
	gex_instance_t *instance = static_cast<gex_instance_t*>(gex_instance);

	instance->intercom->streamer->WritesDone();

	//TODO: Figure out why this causes SEGFAULT
	//instance->intercom->streamer->Finish(); 

	instance->rth->join();
	delete instance->rth;

	instance->wth->join();
	delete instance->wth;

	delete instance->intercom;

	for(auto ptr : instance->audio_bits)
		delete ptr;
	
	for(auto ptr : instance->allocated_strings)
		delete ptr;

	delete instance;
}

void gex_shutdown_module(){
	google::protobuf::ShutdownProtobufLibrary();
}